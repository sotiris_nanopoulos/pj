package com.example.davinci.helloworld;

import android.support.v4.app.FragmentActivity;
import android.os.Bundle;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Marker;

import android.view.View;
import android.widget.TextView;
import android.widget.Toast;


import java.util.ArrayList;

import com.example.davinci.helloworld.coordinatesStruct;

public class MapsActivity extends FragmentActivity implements
        OnInfoWindowClickListener,
        OnMapReadyCallback {


    class MyInfoWindowAdapter implements GoogleMap.InfoWindowAdapter {

        private final View myContentsView;

        MyInfoWindowAdapter(){

            myContentsView = getLayoutInflater().inflate(R.layout.custom_info_contents, null);
        }

        @Override
        public View getInfoContents(Marker marker) {

            TextView tvTitle = ((TextView)myContentsView.findViewById(R.id.title));
            tvTitle.setText(marker.getTitle());
            TextView tvSnippet = ((TextView)myContentsView.findViewById(R.id.snippet));
            tvSnippet.setText(marker.getSnippet());

            return myContentsView;
        }

        @Override
        public View getInfoWindow(Marker marker) {
            // TODO Auto-generated method stub
            return null;
        }

    }


    private GoogleMap mMap;
    private userData  user_data;
    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        user_data = new userData(1);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    @Override
    public void onInfoWindowClick(Marker marker) {

    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setOnInfoWindowClickListener(this);
        mMap.setInfoWindowAdapter(new MyInfoWindowAdapter());
        ArrayList<pidgeon> my_pidgeons = user_data.getPidgeons_locations();
        int i=0;
        for (pidgeon pj:my_pidgeons){
            ArrayList<coordinatesStruct> pidgeon_coordinates =  pj.getCoordinates();
            for (coordinatesStruct coord:pidgeon_coordinates){
                LatLng temp_marker = new LatLng(coord.getLattitude(), coord.getLongitude());
                mMap.addMarker(
                        new MarkerOptions().position(temp_marker).title("A Pigeon")
                                .snippet("Latitude: " + coord.getLattitude() +"\n"+"Longitude: " + coord.getLongitude())
                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_action_name)));
            }
        }
    }
}
