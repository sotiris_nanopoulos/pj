package com.example.davinci.helloworld;

import java.util.ArrayList;


/**
 * Created by davinci on 26/03/2017.
 */


public class userData {

    /*
    For user every user find his pigeons and their locations
    */
    userData( int id ){
        pg_names = new ArrayList<String>();
        pidgeons_locations = new ArrayList<pidgeon>();
        pg_names.add("Pigeon 1");
        pg_names.add("Pigeon 2");
        pg_names.add("Pigeon 3");
        int i=0;
    for (String pg_name:pg_names) {
        pidgeons_locations.add(new pidgeon(pg_name, i));
        i++;
    }
    }

    public ArrayList<String> getPg_names() {
        return pg_names;
    }


    public ArrayList<pidgeon> getPidgeons_locations() {
        return pidgeons_locations;
    }

    private ArrayList<String> pg_names;
    private ArrayList<pidgeon> pidgeons_locations;


}
