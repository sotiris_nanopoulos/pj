package com.example.davinci.helloworld;

import java.util.ArrayList;

/**
 * Created by davinci on 26/03/2017.
 */

public class pidgeon {


    public pidgeon(String name_ ,int offset){

        coordinates = new ArrayList<coordinatesStruct>();
        name=name_;
        coordinates.add(new coordinatesStruct(12,12+offset));
        coordinates.add(new coordinatesStruct(12.5,12+offset));
        coordinates.add(new coordinatesStruct(13,12+offset));
        coordinates.add(new coordinatesStruct(13.5,12+offset));
        coordinates.add(new coordinatesStruct(14,12+offset));
        coordinates.add(new coordinatesStruct(14.5,12+offset));
        coordinates.add(new coordinatesStruct(15,12+offset));
        coordinates.add(new coordinatesStruct(15.5,12+offset));
        coordinates.add(new coordinatesStruct(16,12+offset));
        coordinates.add(new coordinatesStruct(16.5,12+offset));
        coordinates.add(new coordinatesStruct(17,12+offset));
        coordinates.add(new coordinatesStruct(17.5,12+offset));
        coordinates.add(new coordinatesStruct(18,12+offset));
    }

    public String getName() {
        return name;
    }

    public ArrayList<coordinatesStruct> getCoordinates() {
        return coordinates;
    }

    String name;
    private ArrayList<coordinatesStruct> coordinates;
}
