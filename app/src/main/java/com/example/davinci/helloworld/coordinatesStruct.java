package com.example.davinci.helloworld;

/**
 * Created by davinci on 26/03/2017.
 */

public class coordinatesStruct {

    public coordinatesStruct(double lattitude_,double longitude_){

        lattitude=lattitude_;
        longitude=longitude_;
    }

    public double getLattitude() {
        return lattitude;
    }

    public void setLattitude(double lattitude) {
        this.lattitude = lattitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    private double lattitude;
    private double longitude;

}
